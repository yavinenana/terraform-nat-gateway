resource "aws_nat_gateway" "nat" {
//  allocation_id = "${aws_eip.nat.id}"
//  allocation_id = "${var.allocation_id}"
  allocation_id = "${aws_eip.elasticip.id}"
//  subnet_id     = "${aws_subnet.public.id}"
  subnet_id	= "${var.subnet_id}"
  tags = {
    Name = "${var.env}-${var.name}"
  }
}
resource "aws_route" "private_route" {
//  route_table_id = "${var.private_route_table_id}"
  route_table_id = "${var.route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.nat.id}"
}

#resource "aws_eip" "elasticip"{
#        vpc = true
#        lifecycle {
#          prevent_destroy = false
#        }
#}

#Resource "aws_eip_association" "eip_assoc" {
#//  instance_id   = "${aws_instance.ec2_resource.id}"
#  allocation_id = "${aws_eip.elasticip.id}"
#  nat_gateway_id = "${aws_nat_gateway.nat.id}"
#}
